﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookTypeArrow : MonoBehaviour
{
    bool colliding = false;

    private void OnCollisionEnter(Collision other) 
    {
        if(!colliding)
        {
        colliding = true;
        GameObject.Find("HookManager").GetComponent<HookManagerS>().CreateHook(other.contacts[0].point);
        }
    }
    
}
