﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZipLineRenderer : MonoBehaviour
{
   public void RenderZipLine(Vector3 first,Vector3 secound)
   {
       Vector3 diraction = (first - secound);
       Vector3 temp = secound + diraction/2 ;
       temp.y -= 0.8f;
       gameObject.GetComponent<LineRenderer>().SetPosition(0,first);
       gameObject.GetComponent<LineRenderer>().SetPosition(1,temp);
       gameObject.GetComponent<LineRenderer>().SetPosition(2,secound);
   }
}
