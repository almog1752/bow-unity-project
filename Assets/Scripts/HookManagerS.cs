﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookManagerS : MonoBehaviour
{
    bool firstPoint = true;
    Vector3 TempPoint;
    public GameObject ZipLine;
    
    public void CreateHook(Vector3 point)
    {
        if(firstPoint)
        {
            TempPoint= point;
            firstPoint = false;
            return;
        }
        GameObject tempZip = Instantiate(ZipLine,Vector3.zero,Quaternion.identity);
        tempZip.GetComponent<ZipLineRenderer>().RenderZipLine(TempPoint,point);
        firstPoint = true;
    }
}
