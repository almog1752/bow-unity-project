﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineMover : MonoBehaviour
{
    public Transform topBow,botBow,midBow;
    public float force;
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            gameObject.GetComponent<Rigidbody>().AddRelativeForce(Vector3.back*force,ForceMode.Force);
        }
        gameObject.GetComponent<LineRenderer>().SetPosition(0, topBow.position);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, transform.position);
        gameObject.GetComponent<LineRenderer>().SetPosition(2, botBow.position);
        if (transform.localPosition.z> 0)
        {
            Vector3 temp = gameObject.transform.localPosition;
            temp.z -= 0.5f;
            gameObject.transform.localPosition = temp;
        }
    }
}
