﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
     Transform ArrowHeadPos;
     GameObject bow;
     Transform bowString;
    private Rigidbody rb;
    bool hit = false;
    public float ShootOnStart;
    void Start()
    {
        bow = GameObject.Find("BowV2");
        ArrowHeadPos = GameObject.Find("ArrowHeadPos").transform;
        bowString = GameObject.Find("string").transform;
       rb= GetComponent<Rigidbody>();
       rb.centerOfMass = Vector3.forward * 3;
        if(ShootOnStart!=0)Launch(ShootOnStart);
    }

    void Update()
    {
        if (!inAir)
        {
            transform.position = bowString.position;
            transform.LookAt(ArrowHeadPos);
        }
       
            
        
    }
    float timeOfLaunch;
    private void FixedUpdate() {
        if(inAir&&Time.time>timeOfLaunch+0.1f)
        {
            
            if(rb.velocity.magnitude>5) transform.rotation=Quaternion.LookRotation((rb.velocity));

        }
    }    
    bool inAir;
    internal void Launch()
    {
        inAir = true;
    }
    internal void Launch(float force)
    {
        rb.AddRelativeForce(Vector3.forward*force,ForceMode.Impulse);
        timeOfLaunch=Time.time;
        inAir = true;
    }
    

    private void OnCollisionEnter(Collision other)
    {
        if (!hit&&inAir)
        {
            Debug.Log(other.gameObject.name);
            Debug.Log(transform.eulerAngles);
            hit = true;
            var j = gameObject.AddComponent<FixedJoint>();
            if (other.gameObject.GetComponent<Rigidbody>())
                j.connectedBody = other.gameObject.GetComponent<Rigidbody>();
            this.enabled=false;
            Destroy(gameObject,20);
            

        }
    }


}
