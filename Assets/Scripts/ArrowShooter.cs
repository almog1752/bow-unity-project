﻿using System.Collections;
using UnityEngine;

public class ArrowShooter : MonoBehaviour
{
    public float forceCounter = 0;
    public GameObject arrow;
    private void Start() {
    }
public bool Auto;
    void Update()
    {
        if(Auto&&Input.GetMouseButton(0)){
            StartCoroutine(AutomaticBitch());
        }
        else
        {if (Input.GetMouseButtonDown(0))
            LoadArrow();
        if (Input.GetMouseButton(0))
        {
            forceCounter += 60*Time.deltaTime;
        }
        if (Input.GetMouseButtonUp(0))
        {
            shotArrow(forceCounter);
        }}
    }
    GameObject LoadedArrow;
    public void LoadArrow()
    {
        forceCounter = 0;
        LoadedArrow = Instantiate(arrow, transform.position, transform.rotation);
    }
    IEnumerator AutomaticBitch(){
        LoadArrow();

        yield return new WaitForSeconds(0.1f);
        shotArrow(10);
    }
    public void shotArrow(float force)
    {
        force = Mathf.Clamp(force, 3, 20);
        forceCounter = 0;
        LoadedArrow.GetComponentInChildren<Arrow>().Launch(force);
    }

}
