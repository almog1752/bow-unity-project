﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowRotation : MonoBehaviour
{
    Vector3 mouse_pos;
    void Update()
    {
        mouse_pos = Input.mousePosition;
        mouse_pos.z = 1000; //distance from cam
        transform.LookAt(Camera.main.ScreenToWorldPoint(mouse_pos));
    }
}
